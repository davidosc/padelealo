package com.padelealo.app.views.login;

import java.util.Collections;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.flow.component.dependency.JavaScript;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.login.LoginI18n;
import com.vaadin.flow.component.login.LoginOverlay;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;

@Route(LoginView.ROUTE)
@PageTitle("Login | Vaadin CRM")
//@CssImport("./styles/views/login/login-view.css")
@JavaScript("./styles/views/login/vaadin-login-overlay-styles.js")
public class LoginView extends VerticalLayout implements BeforeEnterObserver {

	public static final String ROUTE = "login";

	Logger log = LoggerFactory.getLogger(LoginView.class);
	
	LoginOverlay login = new LoginOverlay();
   
    @Autowired
    public LoginView() {
        // configures login dialog and adds it to the main view
    	
//    	H1 title = new H1();
//    	title.getStyle().set("color", "var(--lumo-base-color)");
//    	title.getStyle().set("color", "#ffbf00");
//    	title.getStyle().set("background-image", "images/pista-de-padel.jpg");
//    	Icon icon = VaadinIcon.VAADIN_H.create();
//    	icon.setSize("30px");
//    	icon.getStyle().set("top", "-4px");
//    	title.add(icon);
//    	title.add(new Text(" My App"));
    	Image img=new Image("images/padel.jpg", "Avatar");
    	img.setSizeFull();
    	login.setTitle("Padelealo");
    	login.setDescription("Aplicación para reservas de padel en tu pista");
//    	login.getElement().getStyle().set("background-image", "images/pista-de-padel.jpg");

    	LoginI18n i18n = LoginI18n.createDefault();
    	i18n.setAdditionalInformation("To close the login form submit non-empty username and password");
    	login.setI18n(i18n);
    	login.setOpened(true);
    	login.setAction("login");
    	add(login);
       
//        login.addLoginListener(e -> { // 
//            try {
//                // try to authenticate with given credentials, should always return not null or throw an {@link AuthenticationException}
//                final Authentication authentication = authenticationManager
//                    .authenticate(new UsernamePasswordAuthenticationToken(e.getUsername(), e.getPassword())); // 
//
//                // if authentication was successful we will update the security context and redirect to the page requested first
//                SecurityContextHolder.getContext().setAuthentication(authentication); // 
//                login.close(); // 
////                UI.getCurrent().navigate(requestCache.resolveRedirectUrl()); // 
//
//            } catch (AuthenticationException ex) { // 
//        // show default error message
//        // Note: You should not expose any detailed information here like "username is known but password is wrong"
//        // as it weakens security.
//                login.setError(true);
//            }
//        });
//        setSizeFull();
//		setAlignItems(Alignment.CENTER);
//		setJustifyContentMode(JustifyContentMode.CENTER);
//		getElement().getStyle().set( "background-image" , "url('./images/pista-de-padel.jpg')" );
    } 
	

//	private LoginForm login = new LoginForm();
//
//	public LoginView() {
//
//		setSizeFull();
//		setAlignItems(Alignment.CENTER);
//		setJustifyContentMode(JustifyContentMode.CENTER);
//		getElement().getStyle().set( "background-image" , "url('./loginImages/pista-de-padel.jpg')" );
////		getElement().getStyle().set("background-image", "url('./loginImages/Prosegur-Background.png')");
////		getElement().getStyle().set( "background-image" , "url('http://www.padellasede.com/wp-content/uploads/2018/04/1TTf_900-1080x675.jpg')" );
////		getElement().getStyle().set( "background-position" , "center" );
//		getElement().getStyle().set("background-repeat", "no-repeat");
//		getElement().getStyle().set("background-size", "cover");
//
//		login.setAction("login");
//
////		add(new H1("Vaadin CRM"), login);
//		add(login);
//	}

	@Override
	public void beforeEnter(BeforeEnterEvent event) { //
		// inform the user about an authentication error
		// (yes, the API for resolving query parameters is annoying...)
		if (!event.getLocation().getQueryParameters().getParameters().getOrDefault("error", Collections.emptyList())
				.isEmpty()) {
			login.setError(true); //
		}
	}
}