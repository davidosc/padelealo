package com.padelealo.app.views.reservation;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Period;
import java.time.temporal.Temporal;
import java.time.temporal.TemporalAmount;
import java.util.ArrayList;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.vaadin.gatanaso.MultiselectComboBox;

import com.padelealo.app.data.entity.Player;
import com.padelealo.app.data.service.PlayerService;
import com.padelealo.app.views.main.MainView;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.datetimepicker.DateTimePicker;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.timepicker.TimePicker;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;

@Route(value = "reservation", layout = MainView.class)
@PageTitle("Reservation")
public class ReservationView extends Div {
	@Autowired
	PlayerService ps;
	Set<Player> lista;
	Set<Player> selected;

	public ReservationView(PlayerService ps) {
		this.ps = ps;
		setId("reservation-view");
		Text tittle = new Text("Realiza tu reserva...");
		
		TimePicker timePicker = new TimePicker();
		timePicker.setLabel("Hora de Reserva");
		timePicker.setMinTime(LocalTime.parse("09:30"));
		timePicker.setMaxTime(LocalTime.parse("21:30"));
		Duration duration=Duration.ofMinutes(90);
		
		DateTimePicker dtp=new DateTimePicker();
		
		timePicker.setStep(duration);
		timePicker.setPlaceholder("Hora de Inicio");
		
		DatePicker datePicker = new DatePicker();
		LocalDate today =LocalDate.now();
		datePicker.setMin(today);
		datePicker.setMax(today.plusWeeks(1));
		datePicker.setLabel("Día de reserva.");
		datePicker.setPlaceholder("Fecha");
		
		VerticalLayout mainvl = new VerticalLayout();

		// create a MultiselectComboBox of Users
		MultiselectComboBox<Player> multiselectComboBox = new MultiselectComboBox();
		multiselectComboBox.setWidth("100%");
		multiselectComboBox.setLabel("Select items");
		multiselectComboBox.setPlaceholder("Choose...");
		ArrayList<Player> users = (ArrayList<Player>) ps.findAll();
		multiselectComboBox.setItems(users);
		multiselectComboBox.setItemLabelGenerator(Player::getFirstName);

		multiselectComboBox.setItems(users);
		multiselectComboBox.addSelectionListener(listener -> {

			if (listener.getAllSelectedItems().size() > 4) {
//				multiselectComboBox.deselect(listener.getOldSelection());
				multiselectComboBox.deselect(listener.getAddedSelection());
				Notification.show("El número de jugadores no puede ser superior a 4.");
			}
			selected = listener.getAllSelectedItems();
//			for (Person p : selected)
//				Notification.show(p.getFirstName() + " " + p.getId());
		});

		mainvl.add(tittle, datePicker,timePicker, multiselectComboBox);

		add(mainvl);
	}

}
