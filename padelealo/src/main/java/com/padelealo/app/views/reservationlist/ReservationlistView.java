package com.padelealo.app.views.reservationlist;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import com.flowingcode.vaadin.addons.fontawesome.FontAwesome;
import com.github.appreciated.card.Card;
import com.github.appreciated.card.ClickableCard;
import com.github.appreciated.card.RippleClickableCard;
import com.github.appreciated.card.action.ActionButton;
import com.github.appreciated.card.action.Actions;
import com.github.appreciated.card.content.IconItem;
import com.github.appreciated.card.content.Item;
import com.github.appreciated.card.label.PrimaryLabel;
import com.github.appreciated.card.label.SecondaryLabel;
import com.github.appreciated.card.label.TitleLabel;
import com.padelealo.app.constants.AppIcons;
import com.padelealo.app.data.entity.Game;
import com.padelealo.app.data.entity.Player;
import com.padelealo.app.data.service.GameService;
import com.padelealo.app.data.service.PlaceService;
import com.padelealo.app.data.service.PlayerService;
import com.padelealo.app.views.main.MainView;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouteAlias;

import github.collaborne.paperfab.SpeedDial;



@Route(value = "", layout = MainView.class)
@PageTitle("Reservation list")
@RouteAlias(value = "", layout = MainView.class)
public class ReservationlistView extends VerticalLayout {
	private static final Logger log = Logger.getLogger(ReservationlistView.class.getName());
	ReservationForm rf;
	AppIcons ai = new AppIcons();
	private GameService gs;
	private PlayerService ps;
	private PlaceService plcs;
	private VerticalLayout cardListLayout = new VerticalLayout();
	List<Game> gamelist = new ArrayList<>();
	Image newGameImage=new Image("images/NewGame.png", "New Game");
	
	Button newGameButton = new Button(newGameImage);
	

	public ReservationlistView(PlayerService ps, PlaceService plcs, GameService gs) {
		this.ps = ps;
		this.plcs = plcs;
		this.gs = gs;
		gamelist = gs.findWeekGames();

		newGameButton=getNewGameButton();
		
		
		rf = new ReservationForm(this, ps, plcs, gs);
		rf.setVisible(false);

		newGameButton.addClickListener(e -> {
			rf.clearAll();
			rf.setGame(new Game());
			if (cardListLayout.isVisible()) {
				cardListLayout.setVisible(false);
				newGameButton.setVisible(false);
				
			}
			else {
				cardListLayout.setVisible(true);
				newGameButton.setVisible(true);
			}
			if (rf.isVisible()) {
				rf.setVisible(false);
				newGameButton.setVisible(false);
			} else
				rf.setVisible(true);
		});

//		addGame.setColor("#1455e0");
//		addGame.setMarginBottom("60px");
		
//		addGame.setIcon(VaadinIcon.PLUS_CIRCLE);
		refreshGameList();
		setAlignItems(Alignment.CENTER);

		add(rf, cardListLayout,newGameButton);
//		setAlignSelf(Alignment.START, addGame);
	}

	

	private Button getNewGameButton() {
		// TODO Auto-generated method stub
		newGameImage.setHeight("75px");
		newGameImage.setWidth("75px");
		newGameButton.getStyle().set("font-color","blue");
		newGameButton.getStyle().set("position", "fixed");
		newGameButton.getStyle().set("bottom", "100px");
		newGameButton.getStyle().set("right", "15px");
		newGameButton.getStyle().set("heigth", "");
		newGameButton.getStyle().set("background-color","#B7EDFF");
		newGameButton.addThemeVariants(ButtonVariant.MATERIAL_OUTLINED);
		return newGameButton;
	}

	protected void refreshGameList() {
		// TODO Auto-generated method stub
		cardListLayout = getNewCardListLayout();
		gamelist = gs.findWeekGames();
		if (gamelist != null) {
//			log.info("El tamaño de la lista es este: " + gamelist.size());
			for (Game game : gamelist) {
				cardListLayout.add(getClickableCard(game));
			}
		}
	}

	private VerticalLayout getNewCardListLayout() {
		// TODO Auto-generated method stub
		cardListLayout.removeAll();
		cardListLayout.setHeightFull();
		return cardListLayout;
	}

	private Component GetGameCard(Game game) {
		// TODO Auto-generated method stub

		VerticalLayout playerslayout1 = new VerticalLayout();
		VerticalLayout playerslayout2 = new VerticalLayout();
		HorizontalLayout playerLayout = new HorizontalLayout();

		playerslayout1.setSizeFull();
		playerslayout2.setSizeFull();
		playerLayout.setSizeFull();
		playerLayout.add(playerslayout1, playerslayout2);

		ArrayList<Player> playerlist = new ArrayList<Player>();
		for (Player p : game.getPlayers())
			playerlist.add(p);
		for (int i = 0; i < playerlist.size(); i++) {
			if (playerlist.get(i) != null)
				if (i < 2)
					playerslayout1.add(new IconItem(ai.getBallIcon(),
							playerlist.get(i).getFirstName() + " " + playerlist.get(i).getLastName(),
							null /* "Player 1 description" */ ));
				else
					playerslayout2.add(new IconItem(ai.getBallIcon(),
							playerlist.get(i).getFirstName() + " " + playerlist.get(i).getLastName(),
							null /* "Player 2 description" */));
		}
//		IconItem ii = new IconItem(new Label(), new Label());
//		for (int i = 0; i < playerlist.size(); i++) {
//			ii = new IconItem(ai.getBallIcon(),
//					playerlist.get(i).getFirstName() + " " + playerlist.get(i).getLastName(),
//					null /* "Player 1 description" */ );
//			ii.setSizeFull();
//			playerLayout.add(ii);
//		}

		Button removeIt = new Button();
		playerLayout.setWidthFull();
		RippleClickableCard card = new RippleClickableCard(componentEvent -> {
			rf.setGame(game);
		}, removeIt, new IconItem(ai.getPadelImage(), game.getGameState().toString(),
				game.getGameDate() + " " + game.getGameTime()), playerLayout);
		card.setAlignSelf(Alignment.CENTER, removeIt);
		card.addClickListener(e -> {
//			Notification.show("Este es el Game ID: " + ((Label) card.getComponentAt(0)).getText());
			ChangeCardListLayoutVisibility();
			rf.setVisible(true);

		});
		removeIt.setIcon(new Icon(VaadinIcon.MINUS_CIRCLE_O));
		removeIt.addClickListener(e -> {
//			Notification.show(
//					((Label) ((RippleClickableCard) removeIt.getParent().get().getParent().get()).getComponentAt(0))
//							.getText());

			removeVerifyDialogo(
					((Label) ((RippleClickableCard) removeIt.getParent().get().getParent().get()).getComponentAt(0))
							.getText());
		});
		Label elemId = new Label(game.getId().toString());
		elemId.setVisible(false);

		card.addComponentAtIndex(0, elemId);
		card.setSizeFull();

		card.setAlignSelf(Alignment.END, removeIt);
		return card;
	}

	private Component GetGameCardNew(Game game) {
		// TODO Auto-generated method stub

		VerticalLayout playerslayout1 = new VerticalLayout();
		VerticalLayout playerslayout2 = new VerticalLayout();
		HorizontalLayout playerLayout = new HorizontalLayout();

		playerslayout1.setSizeFull();
		playerslayout2.setSizeFull();
		playerLayout.setSizeFull();
		playerLayout.add(playerslayout1, playerslayout2);

		ArrayList<Player> playerlist = new ArrayList<Player>();
		for (Player p : game.getPlayers())
			playerlist.add(p);
		for (int i = 0; i < playerlist.size(); i++) {
			if (playerlist.get(i) != null)
				if (i < 2)
					playerslayout1.add(new IconItem(ai.getBallIcon(),
							playerlist.get(i).getFirstName() + " " + playerlist.get(i).getLastName(),
							null /* "Player 1 description" */ ));
				else
					playerslayout2.add(new IconItem(ai.getBallIcon(),
							playerlist.get(i).getFirstName() + " " + playerlist.get(i).getLastName(),
							null /* "Player 2 description" */));
		}
		Button removeIt = new Button();
		RippleClickableCard card = new RippleClickableCard(componentEvent -> {
			rf.setGame(game);
		}, removeIt, new IconItem(ai.getPadelImage(), game.getGameState().toString(),
				game.getGameDate() + " " + game.getGameTime()), playerLayout);

		card.addClickListener(e -> {
//			Notification.show("Este es el Game ID: " + ((Label) card.getComponentAt(0)).getText());
			ChangeCardListLayoutVisibility();
			rf.setVisible(true);
			rf.setGame(findInMemoryGame(((Label) card.getComponentAt(0)).getText()));
		});

		removeIt.setIcon(new Icon(VaadinIcon.MINUS_CIRCLE_O));
		removeIt.addClickListener(e -> {
//			Notification.show(
//					((Label) ((RippleClickableCard) removeIt.getParent().get().getParent().get()).getComponentAt(0))
//							.getText());

			removeVerifyDialogo(
					((Label) ((RippleClickableCard) removeIt.getParent().get().getParent().get()).getComponentAt(0))
							.getText());
		});
		Label elemId = new Label(game.getId().toString());
		elemId.setVisible(false);

		card.addComponentAtIndex(0, elemId);
		card.setSizeFull();

		card.setAlignSelf(Alignment.END, removeIt);
		return card;
	}

	private void removeVerifyDialogo(String text) {
		// TODO Auto-generated method stub

		Dialog dialog = new Dialog();

		dialog.setCloseOnEsc(false);
		dialog.setCloseOnOutsideClick(false);

		Span message = new Span();

		Button confirmButton = new Button("Confirm", event -> {
			message.setText("Confirmed!");
			removeCard(text);
			dialog.close();
		});
		Button cancelButton = new Button("Cancel", event -> {
			message.setText("Cancelled...");
			dialog.close();
		});
		VerticalLayout dvl = new VerticalLayout();
		HorizontalLayout dhl = new HorizontalLayout();
		dvl.setAlignItems(Alignment.CENTER);
		Span question = new Span("Do you really want to remove this Game ???");
		dhl.add(confirmButton, cancelButton);
		dhl.setAlignItems(Alignment.CENTER);
		dvl.add(question, dhl);

		dialog.add(dvl);
		dialog.open();
	}

	private void removeCard(String text) {
		// TODO Auto-generated method stub
//		log.info("Este es el ID del Juego que se va a eliminar: "+text);
		Integer idgame = new Integer(text);
		gs.remove(idgame);
		refreshGameList();
	}

	private Game findInMemoryGame(String text) {
		// TODO Auto-generated method stub
		Game game = new Game();
		if (text != null) {
			if (text != null) {
				for (Game g : gamelist) {
					if (g.getId().compareTo(Integer.getInteger(text)) == 0)
						game = g;
				}
			}
		}
		return game;
	}

	public void ChangeCardListLayoutVisibility() {
		// TODO Auto-generated method stub
		cardListLayout.setVisible(cardListLayout.isVisible() ? false : true);
		newGameButton.setVisible(newGameButton.isVisible() ? false : true);
	}

	private Icon getIcon() {
		// TODO Auto-generated method stub
		return new Icon(VaadinIcon.BOOK_PERCENT);
	}

	public RippleClickableCard sampleCard() {
		RippleClickableCard rcard = new RippleClickableCard(onClick -> {
			/* Handle Card click */}, new TitleLabel("Example Title"), // Follow up with more Components ...
				// if you don't want the title to wrap you can set the whitespace = nowrap
				new TitleLabel("Example Title").withWhiteSpaceNoWrap(), new PrimaryLabel("Some primary text"),
				new SecondaryLabel("Some secondary text"),
				new IconItem(getIcon(), "Icon Item title", "Icon Item description"),
				new Item("Item title", "Item description"), new Image("/images/padel.png", "padel.png"),
				new Actions(new ActionButton("Action 1", event -> {
					/* Handle Action */}), new ActionButton("Action 2", event -> {
						/* Handle Action */})));
		return rcard;
	}

	public RippleClickableCard getRippleCard(String imagePath, String title, String description) {
		Image img = new Image(imagePath, title);
		img.setWidth("50px");
		img.setHeight("50px");
		RippleClickableCard card = new RippleClickableCard(
				componentEvent -> Notification.show("A RippleClickableCard was clicked!"),
				new IconItem(img, title, description));
		card.setWidth("100%");
		return card;
	}

	public RippleClickableCard getClickableCard(Game game) {
		Image img = ai.getPadelImage();
		img.setWidth("35px");
		img.setHeight("50px");
		HorizontalLayout hl = new HorizontalLayout();
		VerticalLayout vl = new VerticalLayout();
		HorizontalLayout titlelayout = new HorizontalLayout();

		VerticalLayout leftLayout = new VerticalLayout();
		VerticalLayout rightLayout = new VerticalLayout();
		HorizontalLayout playersLayout = new HorizontalLayout();
		leftLayout.setSizeFull();
		rightLayout.setSizeFull();
		playersLayout.setSizeFull();
		Button removeIt = new Button();
		removeIt.setIcon(new Icon(VaadinIcon.MINUS_CIRCLE_O));

		boolean lastwasleft = true;
		ArrayList<Player> playerlist = new ArrayList<Player>();
		for (Player p : game.getPlayers())
			playerlist.add(p);
		for (Player player : playerlist) {
			if (lastwasleft) {
				leftLayout.add(createPlayer(player.getFirstName()));
				lastwasleft = false;
			} else {
				rightLayout.add(createPlayer(player.getFirstName()));
				lastwasleft = true;
			}
		}
		playersLayout.add(leftLayout, rightLayout);
		vl.setSizeFull();
		hl.setSizeFull();
		vl.add(img);

//		titlelayout.add(img, new Label(title));

		playersLayout.setPadding(true);

		RippleClickableCard card = new RippleClickableCard(componentEvent -> {
//			Notification.show("A ClickableCard was clickedddddddddddd!" +game.getId());
			rf.setGame(game);

		}, createCardDescription(img, game.getGameDate() + " " + game.getGameTime(), game.getGameState().toString()),
				playersLayout);
		card.addClickListener(e -> {
			ChangeCardListLayoutVisibility();
//			Notification.show("Es visible la forma: "+rf.isVisible());
			rf.setVisible(true);

		});

		card.setWidth("100%");
		Label elemId = new Label(game.getId().toString());
		elemId.setVisible(false);
		card.addComponentAtIndex(0, elemId);

		return card;
	}

	private Label createPlayer(String s) {
		// TODO Auto-generated method stub
		Label label = new Label();
		label.add(ai.getBallIcon());
		label.add(" " + s);
		return label;
	}

	private HorizontalLayout createCardDescription(Image img, String tittle, String details) {
		// TODO Auto-generated method stub
		HorizontalLayout hl = new HorizontalLayout();
		Button removeIt = new Button();
		removeIt.setIcon(new Icon(VaadinIcon.MINUS_CIRCLE_O));
		removeIt.addClickListener(e -> {
//			Notification.show("Hola");
//			Notification.show(
//					((Label) ((RippleClickableCard) removeIt.getParent().get().getParent().get().getParent().get()).getComponentAt(0))
//					.getText());
			removeVerifyDialogo(
					((Label) ((RippleClickableCard) removeIt.getParent().get().getParent().get().getParent().get())
							.getComponentAt(0)).getText());
		});
		Span label = new Span();
		label.add(img);
		label.add(" " + tittle);
		label.add(" " + details);
		label.getElement().getStyle().set("font-size", "18px");
		label.getElement().getStyle().set("font-weight", "bold");
		label.getElement().getStyle().set("color", "#070569");
		label.setWidthFull();
		hl.add(img, label, removeIt);
		hl.setPadding(true);
		hl.setWidthFull();
		hl.setAlignSelf(Alignment.CENTER, label);
		hl.setFlexGrow(1, label);
		return hl;
	}

	public ClickableCard getClickableCard2(String imagePath, String title, String description) {
		Image img = new Image(imagePath, title);
		img.setWidth("50px");
		img.setHeight("50px");
		ClickableCard card = new ClickableCard(componentEvent -> {
			Notification.show("A ClickableCard was clicked!");
		}, new IconItem(img, title, description));
		card.addClickListener(e -> {
			card.setVisible(false);
			rf.setCard(card);
			rf.setVisible(true);
		});
		card.setWidth("100%");

		return card;
	}

	public Card getCard(String imagePath, String title, String description) {
		Image img = new Image(imagePath, title);
		img.setWidth("50px");
		img.setHeight("50px");
		Card card = new Card(new IconItem(img, title, description));
		card.setWidth("100%");

		return card;
	}

}