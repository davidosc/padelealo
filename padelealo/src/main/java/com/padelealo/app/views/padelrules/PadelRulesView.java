package com.padelealo.app.views.padelrules;

import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.padelealo.app.views.main.MainView;

@Route(value = "padelrules", layout = MainView.class)
@PageTitle("Padel Rules")
public class PadelRulesView extends Div {

    public PadelRulesView() {
        setId("padel-rules-view");
        add(new Text("Content placeholder"));
    }

}
