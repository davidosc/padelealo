package com.padelealo.app.views.reservationlist;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.vaadin.gatanaso.MultiselectComboBox;

import com.github.appreciated.card.ClickableCard;
import com.padelealo.app.constants.GameState;
import com.padelealo.app.constants.GameType;
import com.padelealo.app.data.entity.Game;
import com.padelealo.app.data.entity.Place;
import com.padelealo.app.data.entity.Player;
import com.padelealo.app.data.service.GameService;
import com.padelealo.app.data.service.PlaceService;
import com.padelealo.app.data.service.PlayerService;
import com.padelealo.app.views.main.MainView;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.datetimepicker.DateTimePicker;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.orderedlayout.FlexComponent.Alignment;
import com.vaadin.flow.component.timepicker.TimePicker;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;

@Route(value = "reservationform", layout = MainView.class)
@PageTitle("Reservation")
public class ReservationForm extends FormLayout {
	@Autowired
	PlayerService ps;
	Set<Player> lista;
	Set<Player> selected;
	private Button cancel = new Button("Cancel");
	private Button save = new Button("Save");

	@Autowired
	private GameService gameService;
	@Autowired
	private PlaceService plcs;

	ClickableCard card = new ClickableCard();
	MultiselectComboBox<Player> multiselectComboBox = new MultiselectComboBox();
	TimePicker timePicker = new TimePicker();
	DatePicker datePicker = new DatePicker();
	Place place = new Place();
	Checkbox tournament = new Checkbox("PADEL PARTY");
	private Game game = new Game();
	ReservationlistView rlv;

	public ReservationForm(ReservationlistView rlv, PlayerService ps, PlaceService plcs, GameService gameService) {
		this.ps = ps;
		this.plcs = plcs;
		this.gameService = gameService;
		this.rlv = rlv;

		setId("reservation-view");
		Text tittle = new Text("Realiza tu reserva...");

		timePicker.setLabel("Hora de Reserva");
		timePicker.setMinTime(LocalTime.parse("09:30"));
		timePicker.setMaxTime(LocalTime.parse("21:30"));
		Duration duration = Duration.ofMinutes(90);

		DateTimePicker dtp = new DateTimePicker();

		timePicker.setStep(duration);
		timePicker.setPlaceholder("Hora de Inicio");

		LocalDate today = LocalDate.now();
		datePicker.setMin(today);
		datePicker.setMax(today.plusWeeks(1));
		datePicker.setLabel("Día de reserva.");
		datePicker.setPlaceholder("Fecha");

//		VerticalLayout mainvl = new VerticalLayout();

		// create a MultiselectComboBox of Users

		multiselectComboBox.setWidth("100%");
		multiselectComboBox.setLabel("Select items");
		multiselectComboBox.setPlaceholder("Choose...");
		ArrayList<Player> users = (ArrayList<Player>) ps.findAll();
		multiselectComboBox.setItems(users);
		multiselectComboBox.setItemLabelGenerator(Player::getFirstName);
		multiselectComboBox.setClearButtonVisible(true);
		multiselectComboBox.setItems(users);
		multiselectComboBox.addSelectionListener(listener -> {

			if ((listener.getAllSelectedItems().size()) > 4 && (!tournament.getValue())) {
//				multiselectComboBox.deselect(listener.getOldSelection());
				multiselectComboBox.deselect(listener.getAddedSelection());
				Notification.show(
						"El número de jugadores no puede ser superior a 4 a menos que selecciones el modo torneo.");
			}
			selected = listener.getAllSelectedItems();
//			for (Person p : selected)
//				Notification.show(p.getFirstName() + " " + p.getId());
		});
		cancel.addClickListener(e -> {
			setVisible(false);
			rlv.ChangeCardListLayoutVisibility();
			});
		save.addClickListener(e -> {
			save();
		});
		HorizontalLayout buttonsLayout = createButtonLayout();
		tournament.setValue(false);
		tournament.addClickListener(e -> {
			Set<Player> players = multiselectComboBox.getSelectedItems();
			if ((players.size() > 4) && (tournament.getValue() == false)) {
				Notification.show("Elimine jugadores para completar el partido de padel dobles");
				tournament.setValue(true);
			}
		});

		add(datePicker, timePicker, multiselectComboBox, tournament);
		add(buttonsLayout);

	}

	private void save() {
		// TODO Auto-generated method stub
//      gameService.update(binder.getBean());
		Notification.show("The reservation was saved successfully.");
//      clearForm();

		Set<Player> gameplayers = new HashSet<Player>(multiselectComboBox.getSelectedItems());
//      for (Player p: gameplayers)
		place = plcs.getPlace("GVV");
//      Game game = new Game(place, GameType.PADEL, datePicker.getValue(), timePicker.getValue(), gameplayers);
		game.setPlace(place);
		game.setGametype(GameType.PADEL);
		game.setGameDate(datePicker.getValue());
		game.setGameTime(timePicker.getValue());
		game.setPlayers(gameplayers);
		game.setGameState((tournament.getValue() ? GameState.OPEN : GameState.CLOSED));
		gameService.save(game);
		
		rlv.refreshGameList();

		setVisible(false);
		rlv.ChangeCardListLayoutVisibility();
	}

	private HorizontalLayout createButtonLayout() {
		HorizontalLayout buttonLayout = new HorizontalLayout();
		buttonLayout.addClassName("button-layout");
		save.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
		buttonLayout.add(save);
		buttonLayout.add(cancel);
		buttonLayout.setAlignItems(Alignment.CENTER);
		buttonLayout.setSizeFull();
		return buttonLayout;
	}

	public ReservationForm() {
		// TODO Auto-generated constructor stub
	}

	public void setCard(ClickableCard card) {
		// TODO Auto-generated method stub
		this.card = card;
	}

	public void clearAll() {
		datePicker.clear();
		timePicker.clear();
		multiselectComboBox.deselectAll();
		tournament.setValue(false);
	}

	public void setGame(Game game) {
		// TODO Auto-generated method stub
		this.game = game;
		datePicker.setValue(game.getGameDate());
		timePicker.setValue(game.getGameTime());
		multiselectComboBox.deselectAll();
		multiselectComboBox.select(game.getPlayers());
		tournament.setValue((game.getGameState()==GameState.OPEN) ? true : false);
		
	}

}
