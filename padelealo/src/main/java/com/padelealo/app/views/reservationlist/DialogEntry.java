package com.padelealo.app.views.reservationlist;

import java.time.DayOfWeek;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Set;

import org.vaadin.stefan.fullcalendar.Entry;
import org.vaadin.stefan.fullcalendar.ResourceEntry;
import org.vaadin.stefan.fullcalendar.Timezone;

import lombok.Data;

@Data class DialogEntry {
    private String id;
    private String title;
    private String color;
    private String description;
    private LocalDateTime start;
    private LocalDateTime end;
    private boolean allDay;
    private boolean recurring;
    private Set<DayOfWeek> recurringDays;
    private Timezone timezone;
    private ResourceEntry entry;

    public static DialogEntry of(ResourceEntry entry, Timezone timezone) {
        DialogEntry dialogEntry = new DialogEntry();

        dialogEntry.setTimezone(timezone);
        dialogEntry.setEntry(entry);

        dialogEntry.setTitle(entry.getTitle());
        dialogEntry.setColor(entry.getColor());
        dialogEntry.setDescription(entry.getDescription());
        dialogEntry.setAllDay(entry.isAllDay());

        boolean recurring = entry.isRecurring();
        dialogEntry.setRecurring(recurring);

        if (recurring) {
            dialogEntry.setRecurringDays(entry.getRecurringDaysOfWeeks());

            dialogEntry.setStart(entry.getRecurringStartDate(timezone).atTime(entry.getRecurringStartTime()));
            dialogEntry.setEnd(entry.getRecurringEndDate(timezone).atTime(entry.getRecurringEndTime()));
        } else {
            dialogEntry.setStart(entry.getStart(timezone));
            dialogEntry.setEnd(entry.getEnd(timezone));
        }

        return dialogEntry;
    }

    /**
     * Updates the stored entry instance and returns it after updating.
     *
     * @return entry instnace
     */
    Entry updateEntry() {
        entry.setTitle(title);
        entry.setColor(color);
        entry.setDescription(description);
        entry.setAllDay(allDay);
        entry.setRecurring(recurring);

        if (recurring) {
            entry.setRecurringDaysOfWeeks(getRecurringDays());

            entry.setStart((Instant) null);
            entry.setEnd((Instant) null);

            entry.setRecurringStartDate(start.toLocalDate(), timezone);
            entry.setRecurringStartTime(allDay ? null : start.toLocalTime());

            entry.setRecurringEndDate(end.toLocalDate(), timezone);
            entry.setRecurringEndTime(allDay ? null : end.toLocalTime());
        } else {
            entry.setStart(start, timezone);
            entry.setEnd(end, timezone);

            entry.setRecurringStartDate(null);
            entry.setRecurringStartTime(null);
            entry.setRecurringEndDate(null);
            entry.setRecurringEndTime(null);
            entry.setRecurringDaysOfWeeks(null);
        }

        return entry;
    }

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public LocalDateTime getStart() {
		return start;
	}

	public void setStart(LocalDateTime start) {
		this.start = start;
	}

	public LocalDateTime getEnd() {
		return end;
	}

	public void setEnd(LocalDateTime end) {
		this.end = end;
	}

	public boolean isAllDay() {
		return allDay;
	}

	public void setAllDay(boolean allDay) {
		this.allDay = allDay;
	}

	public boolean isRecurring() {
		return recurring;
	}

	public void setRecurring(boolean recurring) {
		this.recurring = recurring;
	}

	public Set<DayOfWeek> getRecurringDays() {
		return recurringDays;
	}

	public void setRecurringDays(Set<DayOfWeek> recurringDays) {
		this.recurringDays = recurringDays;
	}

	public Timezone getTimezone() {
		return timezone;
	}

	public void setTimezone(Timezone timezone) {
		this.timezone = timezone;
	}

	public ResourceEntry getEntry() {
		return entry;
	}

	public void setEntry(ResourceEntry entry) {
		this.entry = entry;
	}
}