package com.padelealo.app.data.entity;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.padelealo.app.constants.GameState;
import com.padelealo.app.constants.GameType;
import com.padelealo.app.data.AbstractEntity;

@Entity
@Table(name = "game")
public class Game extends AbstractEntity {

	
	public Game(Place place, GameType gametype, LocalDate gameDate, LocalTime gameTime, GameState gameState,
			Set<Player> players) {
		super();
		this.place = place;
		this.gametype = gametype;
		this.gameDate = gameDate;
		this.gameTime = gameTime;
		this.gameState = gameState;
		this.players = players;
	}

	public Game() {
		super();
	}

	@ManyToOne
	@JoinColumn(name = "FK_PLACE", nullable = true, updatable = false)
	private Place place;
	private GameType gametype;
	private LocalDate gameDate;
	private LocalTime gameTime;
	private GameState gameState;

	
	@ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "game_players",
            joinColumns = {@JoinColumn(name = "game_id")},
            inverseJoinColumns = {@JoinColumn(name = "player_id")}
    )
	Set<Player> players= new HashSet<>();


	public Place getPlace() {
		return place;
	}

	public void setPlace(Place place) {
		this.place = place;
	}

	public GameType getGametype() {
		return gametype;
	}

	public void setGametype(GameType gametype) {
		this.gametype = gametype;
	}

	public LocalDate getGameDate() {
		return gameDate;
	}

	public void setGameDate(LocalDate gameDate) {
		this.gameDate = gameDate;
	}

	public LocalTime getGameTime() {
		return gameTime;
	}

	public void setGameTime(LocalTime gameTime) {
		this.gameTime = gameTime;
	}

	public void addPlayer(Player player) {
        this.players.add(player);
        player.getGames().add(this);
    }
 
	public void removePlayer(Player player) {
        this.players.remove(player);
        player.getGames().remove(this);
    }
	
	public Set<Player> getPlayers() {
		return players;
	}

	public void setPlayers(Set<Player> players) {
		this.players = players;
	}

	public GameState getGameState() {
		return gameState;
	}

	public void setGameState(GameState gameState) {
		this.gameState = gameState;
	}

	@Override
	public String toString() {
		return "Game [place=" + place + ", gametype=" + gametype + ", gameDate=" + gameDate + ", gameTime=" + gameTime
				+ ", gameState=" + gameState + ", players=" + players + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((gameDate == null) ? 0 : gameDate.hashCode());
		result = prime * result + ((gameState == null) ? 0 : gameState.hashCode());
		result = prime * result + ((gameTime == null) ? 0 : gameTime.hashCode());
		result = prime * result + ((gametype == null) ? 0 : gametype.hashCode());
		result = prime * result + ((place == null) ? 0 : place.hashCode());
		result = prime * result + ((players == null) ? 0 : players.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Game other = (Game) obj;
		if (gameDate == null) {
			if (other.gameDate != null)
				return false;
		} else if (!gameDate.equals(other.gameDate))
			return false;
		if (gameState == null) {
			if (other.gameState != null)
				return false;
		} else if (!gameState.equals(other.gameState))
			return false;
		if (gameTime == null) {
			if (other.gameTime != null)
				return false;
		} else if (!gameTime.equals(other.gameTime))
			return false;
		if (gametype != other.gametype)
			return false;
		if (place == null) {
			if (other.place != null)
				return false;
		} else if (!place.equals(other.place))
			return false;
		if (players == null) {
			if (other.players != null)
				return false;
		} else if (!players.equals(other.players))
			return false;
		return true;
	}

	

}
