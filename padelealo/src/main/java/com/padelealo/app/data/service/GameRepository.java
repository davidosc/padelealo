package com.padelealo.app.data.service;

import org.springframework.data.jpa.repository.JpaRepository;

import com.padelealo.app.data.entity.Game;

public interface GameRepository extends JpaRepository<Game, Integer> {

	
	
}