package com.padelealo.app.data.entity;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.padelealo.app.data.AbstractEntity;

@Entity
@Table(name = "player")
public class Player extends AbstractEntity {

	public Player(String firstName, String lastName, String email, String phone, LocalDate dateOfBirth,
			String occupation, boolean important, String floorLevel, String floorLetter, String floorNumber
			,Set<Game> games
			) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.phone = phone;
		this.dateOfBirth = dateOfBirth;
		this.occupation = occupation;
		this.important = important;
		this.floorLevel = floorLevel;
		this.floorLetter = floorLetter;
		this.floorNumber = floorNumber;
		this.games = games;
	}

	public Player() {
		// TODO Auto-generated constructor stub
	}

	private String firstName;
	private String lastName;
	private String email;
	private String phone;
	private LocalDate dateOfBirth;
	private String occupation;
	private boolean important;
	private String floorLevel;
	private String floorLetter;
	private String floorNumber;

//	@ManyToMany(cascade = {
//            CascadeType.PERSIST,
//            CascadeType.MERGE
//    })
    @ManyToMany(fetch = FetchType.LAZY)
	Set<Game> games= new HashSet<>();

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public LocalDate getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(LocalDate dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getOccupation() {
		return occupation;
	}

	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}

	public boolean isImportant() {
		return important;
	}

	public void setImportant(boolean important) {
		this.important = important;
	}

	public String getFloorLevel() {
		return floorLevel;
	}

	public void setFloorLevel(String floorLevel) {
		this.floorLevel = floorLevel;
	}

	public String getFloorLetter() {
		return floorLetter;
	}

	public void setFloorLetter(String floorLetter) {
		this.floorLetter = floorLetter;
	}

	public String getFloorNumber() {
		return floorNumber;
	}

	public void setFloorNumber(String floorNumber) {
		this.floorNumber = floorNumber;
	}

	public Set<Game> getGames() {
		return games;
	}

	public void setGames(Set<Game> games) {
		this.games = games;
	}

	public void addGame(Game game) {
        this.games.add(game);
        game.getPlayers().add(this);
    }
 
	public void removeGame(Game game) {
        this.games.remove(game);
        game.getPlayers().remove(this);
    }
	
	@Override
	public String toString() {
		return "Player [firstName=" + firstName + ", lastName=" + lastName + ", email=" + email + ", phone=" + phone
				+ ", dateOfBirth=" + dateOfBirth + ", occupation=" + occupation + ", important=" + important
				+ ", floorLevel=" + floorLevel + ", floorLetter=" + floorLetter + ", floorNumber=" + floorNumber
				+ ", games=" + games 
				+ "]";
	}

}
