package com.padelealo.app.data.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.vaadin.artur.helpers.CrudService;

import com.github.javafaker.Faker;
import com.padelealo.app.data.entity.Place;

@Service
public class PlaceService extends CrudService<Place, Integer> {

	private PlaceRepository repository;
	Faker faker = new Faker();

	public PlaceService(@Autowired PlaceRepository repository) {
		this.repository = repository;
	}
	@Override
	protected PlaceRepository getRepository() {
		return repository;
	}

	public void ensureTestData() {
		// TODO Auto-generated method stub
		

	}
	public Place getPlace(String plcname) {
		Place place= new Place();
		place=repository.findByName(plcname);
		return place;
	}
	

	public List<Place> findAll() {
		// TODO Auto-generated method stub
		return repository.findAll();
	}

}
