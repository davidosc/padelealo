package com.padelealo.app.data.service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.Set;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.vaadin.artur.helpers.CrudService;

import com.github.javafaker.Faker;
import com.padelealo.app.constants.GameState;
import com.padelealo.app.constants.GameType;
import com.padelealo.app.data.entity.Game;
import com.padelealo.app.data.entity.Place;
import com.padelealo.app.data.entity.Player;

@Service
public class GameService extends CrudService<Game, Integer> {
	private static final Logger log = Logger.getLogger(GameService.class.getName());
	private GameRepository repository;
	private PlayerRepository prepository;
	private PlaceRepository plrepository;
	private PlaceService plcservice;

	@Autowired
	PlayerService ps;
	Faker faker = new Faker();

	public GameService(@Autowired GameRepository repository, @Autowired PlayerRepository prepository,
			@Autowired PlayerService ps, PlaceRepository plrepository, PlaceService plcservice) {
		this.repository = repository;
		this.prepository = prepository;
		this.ps = ps;
		this.plrepository = plrepository;
		this.plcservice = plcservice;
	}

	@Override
	protected GameRepository getRepository() {
		return repository;
	}

	public void ensureTestData() {
		// TODO Auto-generated method stub
		Game game = new Game();
		Place place = new Place();
		place.setName("GVV");
		place = plrepository.save(place);

		List<Player> players = ps.findAll();

		ArrayList<Game> games = new ArrayList<Game>();

		for (int i = 0; i < 10; i++) {

			Random ran = new Random();

			int low = 10;
			int high = players.size();
			int num1 = 0;
			int num2 = 0;
			int num3 = 0;
			int num4 = 0;
			while ((num1 == num2) || (num2 == num3) || (num3 == num4) || (num1 == num4) || (num1 == num3)
					|| (num2 == num4)) {
				num1 = ran.nextInt(high - low) + low;
				num2 = ran.nextInt(high - low) + low;
				num3 = ran.nextInt(high - low) + low;
				num4 = ran.nextInt(high - low) + low;
			}

			Player jugador1 = players.get(num1);
			Player jugador2 = players.get(num2);
			Player jugador3 = players.get(num3);
			Player jugador4 = players.get(num4);

			Set<Player> gameplayers = new HashSet<Player>();
			gameplayers.add(jugador1);
			gameplayers.add(jugador2);
			gameplayers.add(jugador3);
			gameplayers.add(jugador4);

			game.setPlayers(gameplayers);

			GameState gstate = GameState.OPEN;
			game = new Game(place, GameType.PADEL, LocalDate.now(), LocalTime.now(), gstate, gameplayers);
			game = repository.save(game);

//			games.add(game);
//			jugador1.setGames(games);
//			jugador1.setGames(games);
//			jugador2.setGames(games);
//			jugador3.setGames(games);
//			jugador4.setGames(games);

		}

	}

	public List<Game> findAll() {
		// TODO Auto-generated method stub
		return repository.findAll();
	}

	public List<Game> findWeekGames() {
		// TODO Auto-generated method stub
		return repository.findAll();
	}

	public void save(Game game) {
		if ((game.getGametype() == GameType.PADEL) && (game.getPlayers().size() >= 4))
			game.setGameState(GameState.CLOSED);
		repository.save(game);
	}

	public void remove(Integer gameId) {
		log.info("Este es el valor del gameId: " + gameId);
		Optional<Game> game = repository.findById(gameId);
		if (game.isPresent())
			repository.delete(game.get());
		else
			log.info("The game with Id: " + gameId + " doesn´t exist.");
	}
}
