package com.padelealo.app.data.service;

import com.padelealo.app.data.entity.Player;

import org.springframework.data.jpa.repository.JpaRepository;
import java.time.LocalDate;

public interface PlayerRepository extends JpaRepository<Player, Integer> {

}