package com.padelealo.app.data.service;

import org.springframework.data.jpa.repository.JpaRepository;

import com.padelealo.app.data.entity.Place;

public interface PlaceRepository extends JpaRepository<Place, Integer> {

	Place findByName(String name);
	
	
	
}