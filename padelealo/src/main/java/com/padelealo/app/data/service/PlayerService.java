package com.padelealo.app.data.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.vaadin.artur.helpers.CrudService;

import com.github.javafaker.Faker;
import com.padelealo.app.data.entity.Game;
import com.padelealo.app.data.entity.Player;

@Service
public class PlayerService extends CrudService<Player, Integer> {

	private PlayerRepository repository;
	Faker faker = new Faker();

	public PlayerService(@Autowired PlayerRepository repository) {
		this.repository = repository;
	}

	@Override
	protected PlayerRepository getRepository() {
		return repository;
	}

	public void ensureTestData() {
		// TODO Auto-generated method stub
		for (int i = 0; i < 87; i++) {
			Player persona = new Player(faker.name().firstName(),
			faker.name().lastName(),
			faker.internet().emailAddress(), 
			faker.phoneNumber().cellPhone(), 
			LocalDate.now(),
			faker.company().profession(), 
			true, 
			faker.address().buildingNumber(), 
			faker.address().cityPrefix(),
			faker.address().citySuffix()
			, null
			);
			repository.save(persona);

		}

	}

	public List<Player> findAll() {
		// TODO Auto-generated method stub
		return repository.findAll();
	}

}
