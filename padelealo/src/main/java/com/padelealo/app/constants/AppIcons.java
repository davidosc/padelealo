package com.padelealo.app.constants;

import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.icon.Icon;

public class AppIcons {
	String ballImgPath=new String("/images/tennis-ball.png");
	String ballPadelTittle=new String("Padel");
	
	String padelImgPath=new String("/images/padel.png");
	String padelImgTittle=new String("Padel");

	public Image getBallIcon()
	{
//		Icon ball=new Icon();
		Image img = new Image(ballImgPath, ballPadelTittle);
		img.setWidth("15px");
		img.setHeight("15px");
		
		return img;
	}
	
	public Image getPadelImage()
	{
		Image img = new Image(padelImgPath, padelImgTittle);
		img.setWidth("35px");
		img.setHeight("50px");
		
		return img;
	}
	
}
