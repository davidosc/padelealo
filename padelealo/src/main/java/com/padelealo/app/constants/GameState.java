package com.padelealo.app.constants;

public enum GameState {
	OPEN("The Game is available for other players"),
	CLOSED("The Game is NOT available for other players"),
	FINISHED("The game has been finished");

	final String text;

    /**
     * @param text
     */
	GameState(final String text) {
        this.text = text;
    }

    /* (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return text;
    }

}