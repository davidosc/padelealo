package com.padelealo.app.constants;

public enum GameType {
	PADEL("Padel"),
	PADEL_TOURNAMENT("Padel Tournament"),
	TENIS("Tenis"),
	PINGPONG("PingPong");
	
	final String text;

    /**
     * @param text
     */
	GameType(final String text) {
        this.text = text;
    }

    /* (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return text;
    }

}
