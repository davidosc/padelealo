package com.padelealo.app.constants;

import java.security.SecureRandom;

public enum Lang {

	    es_ES("Spain"), 
	    es_MX("Mexico"),
	    es_AR("Argentina"),
	    es_CO("Argentina"),
	    en_GB("United Kingdom"), 
	    en_US("United States"), 
	    en_CA("Canada"), 
	    pt_PT("Portugal"), 
	    pt_BR("Brasil");
//		standard("United Kingdom");
	 
	    private String url;
	 
	    Lang(String envUrl) {
	        this.url = envUrl;
	    }
	 
	    public String getLang() {
	        return url;
	    }
	    
	    public static Lang getRandomLang() {
            SecureRandom random = new SecureRandom();
            return values()[random.nextInt(values().length)];
        }
	}
