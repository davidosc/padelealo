package com.padelealo.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.padelealo.app.data.service.GameService;
import com.padelealo.app.data.service.PlayerService;

@Component
public class LoadData  implements CommandLineRunner {

	@Autowired
	PlayerService ps;
	@Autowired
	GameService gs;
	 @Override
	    public void run(String... args) throws Exception {
	
			ps.ensureTestData();
			gs.ensureTestData();
		
	}
}
